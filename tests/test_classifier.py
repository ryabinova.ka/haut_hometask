import cv2
import pytest


@pytest.mark.parametrize(
    'path', [
        'data/test_figures.jpg',
        'data/extreme_examples/test_figures_0.png',
        'data/extreme_examples/test_figures_1.png',
    ]
)
def test_classifier(classifier, path):
    img = cv2.imread(path, cv2.COLOR_BGR2GRAY)
    img_colored = classifier.predict(img)

    assert img_colored.any()
    assert len(img_colored.shape) == 3
