import pytest

from src.classifier import ShapeClassifier


@pytest.fixture()
def classifier():
    return ShapeClassifier()
