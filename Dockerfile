FROM python:3.9

RUN apt-get update
RUN apt-get install ffmpeg libsm6 libxext6 -y

COPY requirements.txt requirements.txt
COPY requirements.dev.txt requirements.dev.txt
RUN pip3 install -r requirements.txt
RUN pip3 install -r requirements.dev.txt

CMD ["python3"]