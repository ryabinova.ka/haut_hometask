# Figure classifier

## Project structure

    .
    ├── data                 # Folder with images for tests and results
    ├── src                  # Source files
    │   ├── classifier.py    # Module for figure classifier with predict() method
    │   └── figure.py        # Module, where each contour is processed
    ├── tests                # Test files
    │
    ├── example.ipynb        # Example of the algorithm working
    ├── trial.py             # The same as example.ipynb, but in a script form
    │
    └── Dockerfile           # If you have MacBook with M1, it may be more convinient to use python interpreter in Docker
                             #    This is completely optional

## Run classification

1. create virtual env
```bash
python3 -m venv /path/to/new/virtual/environment
source /path/to/new/virtual/environment/bin/activate
```

2. install dependencies
```bash
pip install -r requirements.txt
```

3. run trial
```bash
export PYTHONPATH='.' && python3 trial.py data/test_figures.jpg
```

Result image will be saved at `data/result/colored.png`

## Run tests

Assumed that you have already run 1 and 2 steps from the "Run classification"

1. install developer dependencies
```bash
pip install -r requirements.dev.txt
```

2. run tests
```bash
export PYTHONPATH='.' && pytest --cov=src tests
```