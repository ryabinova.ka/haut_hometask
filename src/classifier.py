"""Module for shape classification."""

from typing import List

import cv2
import numpy as np

from src.figure import Figure

PIXEL_THRESHOLD = 127
MAX_PIXEL_VALUE = 255


class ShapeClassifier:  # noqa: WPS306 (class without a base class)
    """Classifier class. Here we classify and color figures."""

    def predict(self, img: np.ndarray) -> np.ndarray:
        """Figure classification and coloring.

        Args:
            img: input image

        Returns:
            Output image with colored figures
        """
        img_gray = img[:, :, 0].copy()

        # find figures
        contours = self._get_contours(img_gray)
        figures = [Figure(contour) for contour in contours]

        # return colored figures
        return self._color_figures(img, figures)

    def _get_contours(self, img_gray: np.ndarray) -> np.ndarray:
        """Apply threshold and find contours.

        Args:
            img_gray: gray image with shape (h, w)

        Returns:
            contours
        """
        # preprocess image
        _, binary_img = cv2.threshold(
            img_gray,
            PIXEL_THRESHOLD,
            MAX_PIXEL_VALUE,
            cv2.THRESH_BINARY,
        )

        # find contours
        contours, _ = cv2.findContours(
            binary_img,
            cv2.RETR_EXTERNAL,
            cv2.CHAIN_APPROX_SIMPLE,
        )
        return contours

    def _color_figures(
        self,
        img: np.ndarray,
        figures: List[Figure],
    ) -> np.ndarray:
        """Assign colors and return colored image.

        Args:
            img: original image with shape (h, w, 3)
            figures: list of Figure objects

        Returns:
            colored image
        """
        for figure in figures:
            cv2.fillPoly(
                img,
                pts=[figure.contour],
                color=figure.figure_color.value,
            )
        return img
