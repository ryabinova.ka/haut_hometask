"""Module for Figure class. Here we save figures and classify it."""

from enum import Enum

import cv2
import numpy as np

EPSILON_CONST = 0.02


class FigureColor(Enum):
    """Structure for constants."""

    triangle = (0, 0, 255)
    rectangle = (255, 0, 0)
    circle = (0, 255, 0)


class Figure:  # noqa: WPS306 (class without a base class)
    """Figure class."""

    def __init__(self, contour: np.ndarray) -> None:
        """Save contour and assign color to it.

        Args:
            contour: coordinates of the contour
        """
        self.contour = contour
        self.figure_color = self._assign_figure_color()

    def _assign_figure_color(self) -> FigureColor:
        """Classify figure on 3 classes.

        Returns:
            Classified FigureColor value
        """
        epsilon = EPSILON_CONST * cv2.arcLength(self.contour, closed=True)
        lines = cv2.approxPolyDP(self.contour, epsilon=epsilon, closed=True)

        if lines.shape[0] == 3:
            return FigureColor.triangle
        elif lines.shape[0] == 4:
            return FigureColor.rectangle
        return FigureColor.circle
