import argparse

import cv2

from src.classifier import ShapeClassifier

parser = argparse.ArgumentParser(description='Classify figures on image')
parser.add_argument('path', type=str, help='path to image')
args = parser.parse_args()

img = cv2.imread(args.path, cv2.COLOR_BGR2GRAY)

classifier = ShapeClassifier()
img_colored = classifier.predict(img)

cv2.imwrite('data/result/colored.png', img_colored)
